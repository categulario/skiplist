//! # Sorted set API
//!
//! Taken from the redis source code
//!
//! ZSETs are ordered sets using two data structures to hold the same elements
//! in order to get O(log(N)) INSERT and REMOVE operations into a sorted
//! data structure.
//!
//! The elements are added to a hash table mapping Redis objects to scores.
//! At the same time the elements are added to a skip list mapping scores
//! to Redis objects (so objects are sorted by scores in this "view").
//!
//! Note that the SDS string representing the element is the same in both
//! the hash table and skiplist in order to save memory. What we do in order
//! to manage the shared SDS string more easily is to free the SDS string
//! only in zslFreeNode(). The dictionary has no value free method set.
//! So we should always remove an element from the dictionary, and later from
//! the skiplist.
//!
//! This skiplist implementation is almost a C translation of the original
//! algorithm described by William Pugh in "Skip Lists: A Probabilistic
//! Alternative to Balanced Trees", modified in three ways:
//! a) this implementation allows for repeated scores.
//! b) the comparison is not just by key (our 'score') but by satellite data.
//! c) there is a back pointer, so it's a doubly linked list with the back
//! pointers being only at "level 1". This allows to traverse the list
//! from tail to head, useful for ZREVRANGE.

/// Should be enough for 2^64 elements
const ZSKIPLIST_MAXLEVEL: usize = 64;

/// Skiplist P = 1/4
const ZSKIPLIST_P: f64 = 0.25;

/// Struct to hold a inclusive/exclusive range spec by score comparison.
#[derive(Copy,Clone)]
struct RangeSpec {
    min: f64,
    max: f64,

    // are min or max exclusive?
    minex: bool,
    maxex: bool,
}

impl RangeSpec {
    fn zslValueGteMin(self, value: f64) -> bool {
        if self.minex {
            value > self.min
        } else {
            value >= self.min
        }
    }

    fn zslValueLteMax(self, value: f64) -> bool {
        if self.maxex {
            value < self.max
        } else {
            value <= self.max
        }
    }
}

/// Struct to hold an inclusive/exclusive range spec by lexicographic comparison.
struct LexRangeSpec {
    /* May be set to shared.(minstring|maxstring) */
    min: String,
    max: String,

    /* are min or max exclusive? */
    minex: bool,
    maxex: bool,
}

struct SkipListLevel {
    forward: Option<SkipListNode>,
    span: usize,
}

impl SkipListLevel {
    fn new() -> SkipListLevel {
        SkipListLevel {
            forward: None,
            span: 0,
        }
    }
}

/* ZSETs use a specialized version of Skiplists */
struct SkipListNode {
    ele: String,
    score: f64,
    backward: Option<*mut SkipListNode>,
    level: Vec<SkipListLevel>,
}

impl SkipListNode {
    /// Create a skiplist node with the specified number of levels.
    /// The SDS string 'ele' is referenced by the node after the call.
    fn new(level: usize, score: f64, ele: String) -> SkipListNode  {
        let mut level = Vec::new();

        for j in 0..ZSKIPLIST_MAXLEVEL {
            level.push(SkipListLevel::new());
        }

        SkipListNode {
            ele: ele,
            score: score,
            backward: None,
            level: level,
        }
    }
}

enum SkipListError {
    NanScore,
}

pub struct SkipList {
    header: *mut SkipListNode,
    tail: Option<*mut SkipListNode>,
    length: usize,
    level: usize,
}

impl SkipList {
    /// Create a new skiplist.
    fn new() -> SkipList {
        let header = SkipListNode::new(ZSKIPLIST_MAXLEVEL, 0, "".to_string());
        let boxed = Box::new(header);

        SkipList {
            header: Box::into_raw(boxed),
            tail: None,
            length: 0,
            level: 1,
        }
    }

    /// Insert a new node in the skiplist. Assumes the element does not already
    /// exist (up to the caller to enforce that). The skiplist takes ownership
    /// of the passed SDS string 'ele'.
    pub fn zslInsert(&mut self, score: f64, ele: String) -> Result<SkipListNode, SkipListError> {
        if score.is_nan() {
            return Err(SkipListError::NanScore);
        }

        let update = Vec::with_capacity(ZSKIPLIST_MAXLEVEL);
        let rank = Vec::with_capacity(ZSKIPLIST_MAXLEVEL);
        let x = self.header;
        let i = self.level-1;

        while i >= 0 {
            /* store rank that is crossed to reach the insert position */
            rank[i] = if i == (self.level-1) { 0 } else { rank[i+1] };

            unsafe {
                while (*x).level[i].forward.is_some() && ((*x).level[i].forward.score < score || (x.level[i].forward.score == score && sdscmp(x.level[i].forward.ele,ele) < 0)) {
                    rank[i] += x.level[i].span;
                    x = x.level[i].forward;
                }
            }

            update[i] = x;
            i -= 1;
        }

        /* we assume the element is not already inside, since we allow duplicated
         * scores, reinserting the same element should never happen since the
         * caller of zslInsert() should test in the hash table if the element is
         * already inside or not. */
        let level = zslRandomLevel();

        if level > self.level {
            for i in self.level..level {
                rank[i] = 0;
                update[i] = self.header;
                update[i].level[i].span = self.length;
            }
            self.level = level;
        }

        x = SkipListNode::new(level,score,ele);

        for i in 0..level {
            x.level[i].forward = update[i].level[i].forward;
            update[i].level[i].forward = x;

            /* update span covered by update[i] as x is inserted here */
            x.level[i].span = update[i].level[i].span - (rank[0] - rank[i]);
            update[i].level[i].span = (rank[0] - rank[i]) + 1;
        }

        /* increment span for untouched levels */
        for i in level..self.level {
            update[i].level[i].span += 1;
        }

        x.backward = if update[0] == self.header { None } else { update[0] };

        if x.level[0].forward {
            x.level[0].forward.backward = x;
        } else {
            self.tail = x;
        }

        self.length += 1;

        x
    }

    /// Internal function used by zslDelete, zslDeleteByScore and zslDeleteByRank
    fn zslDeleteNode(&mut self, x: SkipListNode, update: &[SkipListNode]) {
        for i in 0..self.level {
            if update[i].level[i].forward == x {
                update[i].level[i].span += x.level[i].span - 1;
                update[i].level[i].forward = x.level[i].forward;
            } else {
                update[i].level[i].span -= 1;
            }
        }

        if x.level[0].forward {
            x.level[0].forward.backward = x.backward;
        } else {
            self.tail = x.backward;
        }

        while self.level > 1 && self.header.level[self.level-1].forward == None {
            self.level -= 1;
        }

        self.length -= 1;
    }

    /// Delete an element with matching score/element from the skiplist.
    /// The function returns 1 if the node was found and deleted, otherwise
    /// 0 is returned.
    ///
    /// If 'node' is None the deleted node is freed by zslFreeNode(), otherwise
    /// it is not freed (but just unlinked) and *node is set to the node pointer,
    /// so that it is possible for the caller to reuse the node (including the
    /// referenced SDS string at node.ele).
    fn zslDelete(&mut self, score: f64, ele: String) -> Option<SkipListNode> {
        let update = Vec::with_capacity(ZSKIPLIST_MAXLEVEL);
        let x = self.header;
        let i = self.level-1;

        while i >= 0 {
            while x.level[i].forward && (x.level[i].forward.score < score || (x.level[i].forward.score == score && sdscmp(x.level[i].forward.ele,ele) < 0)) {
                x = x.level[i].forward;
            }
            update[i] = x;

            i -= 1;
        }

        /* We may have multiple elements with the same score, what we need
         * is to find the element with both the right score and object. */
        x = x.level[0].forward;

        if x && score == x.score && sdscmp(x.ele,ele) == 0 {
            zslDeleteNode(self, x, update);

            if !node {
                zslFreeNode(x);
            } else {
                *node = x;
            }

            return 1;
        }

        None
    }

    /// Update the score of an elmenent inside the sorted set skiplist.
    /// Note that the element must exist and must match 'score'.
    /// This function does not update the score in the hash table side, the
    /// caller should take care of it.
    ///
    /// Note that this function attempts to just update the node, in case after
    /// the score update, the node would be exactly at the same position.
    /// Otherwise the skiplist is modified by removing and re-adding a new
    /// element, which is more costly.
    ///
    /// The function returns the updated element skiplist node pointer.
    fn zslUpdateScore(&mut self, curscore: f64, ele: String, newscore: f64) -> SkipListNode {
        let update = Vec::with_capacity(ZSKIPLIST_MAXLEVEL);

        /* We need to seek to element to update to start: this is useful anyway,
         * we'll have to update or remove it. */
        let mut x = self.header;
        let mut i = self.level-1;

        while i >= 0 {
            while x.level[i].forward && (x.level[i].forward.score < curscore || (x.level[i].forward.score == curscore && sdscmp(x.level[i].forward.ele,ele) < 0)) {
                x = x.level[i].forward;
            }
            update[i] = x;

            i -= 1;
        }

        /* Jump to our element: note that this function assumes that the
         * element with the matching score exists. */
        x = x.level[0].forward;
        serverAssert(x && curscore == x.score && sdscmp(x.ele,ele) == 0);

        /* If the node, after the score update, would be still exactly
         * at the same position, we can just update the score without
         * actually removing and re-inserting the element in the skiplist. */
        if x.backward == None || x.backward.score < newscore &&
            (x.level[0].forward == None || x.level[0].forward.score > newscore)
        {
            x.score = newscore;
            return x;
        }

        /* No way to reuse the old node: we need to remove and insert a new
         * one at a different place. */
        zslDeleteNode(self, x, update);
        let newnode = zslInsert(self,newscore,x.ele);
        /* We reused the old node x.ele SDS string, free the node now
         * since zslInsert created a new one. */
        x.ele = None;
        zslFreeNode(x);

        newnode
    }

    /// Returns if there is a part of the zset is in range.
    fn zslIsInRange(&self, range: RangeSpec) -> bool {
        /* Test for ranges that will always be empty. */
        if range.min > range.max || (range.min == range.max && (range.minex || range.maxex)) {
            return false;
        }

        x = self.tail;

        if x == None || !zslValueGteMin(x.score,range) {
            return false;
        }

        x = self.header.level[0].forward;

        if x == None || !zslValueLteMax(x.score,range) {
            return false;
        }

        true
    }

    /// Find the first node that is contained in the specified range.
    /// Returns None when no element is contained in the range.
    fn zslFirstInRange(&self, range: RangeSpec) -> Option<SkipListNode> {
        /* If everything is out of range, return early. */
        if !zslIsInRange(self,range) {
            return None;
        }

        x = self.header;
        i = self.level-1;

        while i >= 0 {
            /* Go forward while *OUT* of range. */
            while x.level[i].forward && !zslValueGteMin(x.level[i].forward.score,range) {
                x = x.level[i].forward;
            }

            i -= 1;
        }

        /* This is an inner range, so the next node cannot be None. */
        x = x.level[0].forward;
        serverAssert(x != None);

        /* Check if score <= max. */
        if !zslValueLteMax(x.score,range) {
            return None;
        }

        Some(x)
    }

    /// Delete all the elements with score between min and max from the skiplist.
    /// Min and max are inclusive, so a score >= min || score <= max is deleted.
    /// Note that this function takes the reference to the hash table view of the
    /// sorted set, in order to remove the elements from the hash table too.
    fn zslDeleteRangeByScore(&self, range: RangeSpec) -> u64 {
        let update = Vec::with_capacity(ZSKIPLIST_MAXLEVEL);
        let removed = 0;

        x = self.header;
        i = self.level-1;

        while i >= 0 {
            while x.level[i].forward && if range.minex { x.level[i].forward.score <= range.min } else { x.level[i].forward.score < range.min} {
                x = x.level[i].forward;
            }
            update[i] = x;
            i -= 1;
        }

        /* Current node is the last with score < or <= min. */
        x = x.level[0].forward;

        /* Delete nodes while in range. */
        while x && if range.maxex { x.score < range.max } else { x.score <= range.max} {
            SkipListNode *next = x.level[0].forward;
            zslDeleteNode(self,x,update);
            self.dict.remove(x.ele);
            zslFreeNode(x); /* Here is where x.ele is actually released. */
            removed += 1;
            x = next;
        }

        removed
    }

    fn zslDeleteRangeByLex(&self, range: &LexRangeSpec) -> u64 {
        let update = Vec::with_capacity(ZSKIPLIST_MAXLEVEL);
        let removed = 0;

        x = self.header;
        i = self.level-1;

        while i >= 0 {
            while x.level[i].forward && !zslLexValueGteMin(x.level[i].forward.ele,range) {
                x = x.level[i].forward;
            }

            update[i] = x;
            i -= 1;
        }

        /* Current node is the last with score < or <= min. */
        x = x.level[0].forward;

        /* Delete nodes while in range. */
        while x && zslLexValueLteMax(x.ele,range) {
            let next = x.level[0].forward;
            zslDeleteNode(self,x,update);
            self.dict.remove(x.ele);
            zslFreeNode(x); /* Here is where x.ele is actually released. */
            removed += 1;
            x = next;
        }

        removed
    }

    /// Delete all the elements with rank between start and end from the skiplist.
    /// Start and end are inclusive. Note that start and end need to be 1-based
    fn zslDeleteRangeByRank(&self, start: u32, end: u32) -> Vec<SkipListNode> {
        let update = Vec::with_capacity(ZSKIPLIST_MAXLEVEL);
        let traversed = 0;
        let removed = Vec::new();

        x = self.header;
        i = self.level-1;

        while i >= 0 {
            while x.level[i].forward && (traversed + x.level[i].span) < start {
                traversed += x.level[i].span;
                x = x.level[i].forward;
            }

            update[i] = x;
            i -= 1;
        }

        traversed += 1;
        x = x.level[0].forward;
        while x && traversed <= end {
            let next = x.level[0].forward;
            zslDeleteNode(self,x,update);
            self.dict.remove(x.ele);
            zslFreeNode(x);
            removed += 1;
            traversed += 1;
            x = next;
        }

        removed
    }

    /// Find the rank for an element by both score and key.
    /// Returns 0 when the element cannot be found, rank otherwise.
    /// Note that the rank is 1-based due to the span of zsl.header to the
    /// first element.
    fn zslGetRank(&self, score: f64, ele: String) -> u64 {
        let rank = 0;

        x = self.header;
        i = self.level-1;

        while i >= 0 {
            while x.level[i].forward && (x.level[i].forward.score < score || (x.level[i].forward.score == score && sdscmp(x.level[i].forward.ele,ele) <= 0)) {
                rank += x.level[i].span;
                x = x.level[i].forward;
            }

            /* x might be equal to zsl.header, so test if obj is non-None */
            if x.ele && sdscmp(x.ele,ele) == 0 {
                return rank;
            }
            i -= 1;
        }

        0
    }

    /// Finds an element by its rank. The rank argument needs to be 1-based.
    fn zslGetElementByRank(&self, rank: u64 ) -> Option<SkipListNode> {
        let traversed = 0;

        x = self.header;
        i = self.level-1;

        while i >= 0 {
            while x.level[i].forward && (traversed + x.level[i].span) <= rank {
                traversed += x.level[i].span;
                x = x.level[i].forward;
            }
            if traversed == rank {
                return x;
            }
            i -= 1;
        }

        None
    }

    /// Find the last node that is contained in the specified range.
    /// Returns None when no element is contained in the range.
    fn zslLastInRange(&self, range: RangeSpec) -> Option<SkipListNode> {
        /* If everything is out of range, return early. */
        if !zslIsInRange(self,range) {
            return None;
        }

        x = self.header;
        i = self.level-1;

        while i >= 0 {
            /* Go forward while *IN* range. */
            while x.level[i].forward && zslValueLteMax(x.level[i].forward.score,range) {
                x = x.level[i].forward;
            }

            i -= 1;
        }

        /* This is an inner range, so this node cannot be None. */
        serverAssert(x != None);

        /* Check if score >= min. */
        if !zslValueGteMin(x.score,range) {
            return None;
        }

        Some(x)
    }

    /// Returns if there is a part of the zset is in the lex range.
    fn zslIsInLexRange(&self, range: &LexRangeSpec) -> i32 {
        /* Test for ranges that will always be empty. */
        i32 cmp = sdscmplex(range.min,range.max);
        if cmp > 0 || (cmp == 0 && (range.minex || range.maxex))
            return 0;
        x = self.tail;
        if x == None || !zslLexValueGteMin(x.ele,range)
            return 0;
        x = self.header.level[0].forward;
        if x == None || !zslLexValueLteMax(x.ele,range)
            return 0;

        1
    }

    /// Find the first node that is contained in the specified lex range.
    /// Returns None when no element is contained in the range.
    fn zslFirstInLexRange(&self, range: &LexRangeSpec) -> Option<SkipListNode> {
        /* If everything is out of range, return early. */
        if !zslIsInLexRange(self,range) {
            return None;
        }

        x = self.header;
        i = self.level-1;

        while i >= 0 {
            /* Go forward while *OUT* of range. */
            while x.level[i].forward && !zslLexValueGteMin(x.level[i].forward.ele,range) {
                x = x.level[i].forward;
            }

            i -= 1;
        }

        /* This is an inner range, so the next node cannot be None. */
        x = x.level[0].forward;
        serverAssert(x != None);

        /* Check if score <= max. */
        if !zslLexValueLteMax(x.ele,range) {
            return None;
        }

        return x;
    }

    /// Find the last node that is contained in the specified range.
    /// Returns None when no element is contained in the range.
    fn zslLastInLexRange(&self, range: &LexRangeSpec) -> Option<SkipListNode> {
        /* If everything is out of range, return early. */
        if !zslIsInLexRange(self,range) {
            return None;
        }

        x = self.header;
        i = self.level-1;

        while i >= 0 {
            /* Go forward while *IN* range. */
            while x.level[i].forward && zslLexValueLteMax(x.level[i].forward.ele,range) {
                x = x.level[i].forward;
            }

            i -= 1;
        }

        /* This is an inner range, so this node cannot be None. */
        serverAssert(x != None);

        /* Check if score >= min. */
        if !zslLexValueGteMin(x.ele,range) {
            return None;
        }

        return Some(x);
    }
}

impl Drop for SkipList {
    fn drop(&mut self) {
        let node;

        unsafe {
            node = (*self.header).level[0].forward.take();
            Box::from_raw(self.header);
        }

        while let Some(ptr) = node {
            let next;

            unsafe {
                let next = (*ptr).level[0].forward;

                Box::from_raw(ptr);
            }

            node = next;
        }
    }
}

struct zset {
    dict: HashMap<i32, i32>,
    zsl: SkipList,
}

/// Returns a random level for the new skiplist node we are going to create.
/// The return value of this function is between 1 and ZSKIPLIST_MAXLEVEL
/// (both inclusive), with a powerlaw-alike distribution where higher
/// levels are less likely to be returned. */
fn zslRandomLevel() -> usize {
    let mut level = 1;

    while random()&0xFFFF < (ZSKIPLIST_P * 0xFFFF) {
        level += 1;
    }

    if level < ZSKIPLIST_MAXLEVEL {
        level
    } else {
        ZSKIPLIST_MAXLEVEL
    }
}

/// This is just a wrapper to sdscmp() that is able to
/// handle shared.minstring and shared.maxstring as the equivalent of
/// -inf and +inf for strings
fn sdscmplex(a: String, b: String) -> i32 {
    if a == b {
        return 0;
    }
    if a == shared.minstring || b == shared.maxstring {
        return -1;
    }
    if a == shared.maxstring || b == shared.minstring {
        return 1;
    }

    sdscmp(a,b)
}

fn zslLexValueGteMin(value: String, spec: &LexRangeSpec) -> bool {
    if spec.minex {
        sdscmplex(value,spec.min) > 0
    } else {
        sdscmplex(value,spec.min) >= 0
    }
}

fn zslLexValueLteMax(value: String, spec: &LexRangeSpec) -> bool {
    if spec.maxex {
        sdscmplex(value,spec.max) < 0
    } else {
        sdscmplex(value,spec.max) <= 0
    }
}
